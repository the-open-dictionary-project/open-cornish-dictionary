package cymru.prv.dictionary.cornish.tenses;

import cymru.prv.dictionary.cornish.CornishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class Imperfect extends CornishVerbTense {

    public Imperfect(CornishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected boolean useSecondForm(CornishVerb verb) {
        return verb.getNormalForm().matches(".*(el|es|he|i)$");
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        if(useSecondForm)
            return Collections.singletonList(applyWithAffectation("yn"));
        if(endsInYa)
            return Collections.singletonList(apply("yen"));
        return Collections.singletonList(apply("en"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        if(useSecondForm)
            return Collections.singletonList(applyWithAffectation("ys"));
        if(endsInYa)
            return Collections.singletonList(apply("yes"));
        return Collections.singletonList(apply("es"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        if(useSecondForm)
            return Collections.singletonList(applyWithAffectation("i"));
        if(endsInYa)
            return Collections.singletonList(apply("ya"));
        return Collections.singletonList(apply("a"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return getDefaultSingularFirst();
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        Function<String, String> function = useSecondForm ? this::applyWithAffectation : this::apply;
        if(endsInYa)
            return Collections.singletonList(function.apply("yewgh"));
        return Collections.singletonList(function.apply("ewgh"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        Function<String, String> function = useSecondForm ? this::applyWithAffectation : this::apply;
        if(endsInYa)
            return Collections.singletonList(function.apply("yens"));
        return Collections.singletonList(function.apply("ens"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyWithAffectation("ys"));
    }
}
