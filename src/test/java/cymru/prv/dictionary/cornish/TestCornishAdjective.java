package cymru.prv.dictionary.cornish;

import cymru.prv.dictionary.common.Dictionary;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * General tests to ensure that the
 * Cornish adjectives inflects correctly
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class TestCornishAdjective {

    @Test
    public void testCornishWithNoDetails(){
        var adj = new CornishAdjective(new JSONObject().put("normalForm", "test"));
        assertFalse(adj.toJson().has("inflections"));
        assertIterableEquals(List.of("test"), adj.getVersions());
    }


}
