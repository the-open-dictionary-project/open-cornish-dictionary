package cymru.prv.dictionary.cornish.tenses;

import cymru.prv.dictionary.cornish.CornishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class Preterite extends CornishVerbTense {

    public Preterite(CornishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected boolean useSecondForm(CornishVerb verb) {
        return verb.getNormalForm().endsWith("el");
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(applyWithAffectation("is"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(applyWithAffectation("sys"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        if(useSecondForm)
            return Collections.singletonList(applyWithAffectation("is"));
        if(endsInYa)
            return Collections.singletonList(apply("yas"));
        return Collections.singletonList(apply("as"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyWithAffectation("syn"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Collections.singletonList(applyWithAffectation("sowgh"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(apply("sons"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return getDefaultSingularThird();
    }
}
