package cymru.prv.dictionary.cornish;

import cymru.prv.dictionary.common.Conjugation;
import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.cornish.tenses.*;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class CornishVerb extends CornishWord  {

    private final String stem;
    private final String raisedStem;
    private final String affection;
    private final Map<String, CornishVerbTense> tenses = new HashMap<>();
    private Affection infinitiveAffection = Affection.BASE;


    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj a JSON object containing at least
     *            the field "normalForm". The field
     *            "notes" is optional
     */
    public CornishVerb(JSONObject obj) {
        super(obj, WordType.verb);

        affection = obj.optString("affection", null);

        stem = obj.optString("stem", generateStem(getNormalForm()));
        raisedStem = generateAffectionStem(stem, Affection.RAISED);

        addRequiredTense(obj, "present_future", PresentFuture::new);
        addRequiredTense(obj, "imperfect", Imperfect::new);
        addRequiredTense(obj, "preterite", Preterite::new);
        addRequiredTense(obj, "conditional", Conditional::new);
        addRequiredTense(obj, "subjunctive_present_future", SubjunctivePresentFuture::new);
        addRequiredTense(obj, "subjunctive_imperfect", SubjunctiveImperfect::new);
        addRequiredTense(obj, "imperative", Imperative::new);

        addOptionalTense(obj, "present", SpecialVerbTense::new);
        addOptionalTense(obj, "present_short", SpecialVerbTense::new);
        addOptionalTense(obj, "present_long", SpecialVerbTense::new);
        addOptionalTense(obj, "imperfect_short", SpecialVerbTense::new);
        addOptionalTense(obj, "imperfect_long", SpecialVerbTense::new);
        addOptionalTense(obj, "future", PresentFuture::new);
        addOptionalTense(obj, "perfect", SpecialVerbTense::new);
    }

    public String generateAffectionStem(String stem, Affection target){
        return generateAffectionStem(stem, affection, target);
    }

    /**
     * Takes a stem, then preforms affectation on the stem to the degree specified.
     * The affection string contains the information about what affection should occur.
     * This string is formatted like base/raised(/double raised).
     *
     * For example: a/e, aa/ee, or o/e/y
     *
     * @param stem The stem to preform affection on
     * @param affection the definition of the affection to preform
     * @param target which affection to apply
     * @return the string with the affections applied
     */
    private String generateAffectionStem(String stem, String affection, Affection target){
        if(affection == null)
            return stem;
        String[] parts = affection.split("/");
        String baseVowel = parts[0];
        String raised = parts[Math.min(parts.length - 1, target.degree)];

        return changeVowels(stem, baseVowel, raised);
    }

    private String changeVowels(String stem, String from, String to){

        int vowelsLeft = from.length();
        char currentFrom = from.charAt(vowelsLeft-1);
        char currentTo = to.charAt(vowelsLeft-1);

        char[] affectedStem = stem.toCharArray();
        for(int i = stem.length() - 1; i >= 0 && vowelsLeft > 0; i--){
            if(stem.charAt(i) == currentFrom) {
                affectedStem[i] = currentTo;
                vowelsLeft--;
                if(vowelsLeft > 0){
                    currentFrom = from.charAt(vowelsLeft-1);
                    currentTo = to.charAt(vowelsLeft-1);
                }
            }
        }

        return String.valueOf(affectedStem);
    }

    private String generateStem(String normalForm) {
        // Remove the ending
        var stem = normalForm.replaceFirst("(yon|yas|es|el|os|òs|ya|he|a|e|o|i|u)$", "");

        // Check if it is already raised
        if(affection != null){
            String[] affections = affection.split("/");
            var base = affections[0].charAt(0);
            var raised = affections[1].charAt(0);

            // If it is raised, return the stem to the base form
            if(stem.lastIndexOf(raised) > stem.lastIndexOf(base)) {
                infinitiveAffection = Affection.RAISED;
                return changeVowels(stem, affections[1], affections[0]);
            }
        }

        return stem;
    }

    private void addRequiredTense(JSONObject obj, String key, BiFunction<CornishVerb, JSONObject, CornishVerbTense> func){
        if(obj.has(key)) {
            Conjugation conjugation = func.apply(this, obj.getJSONObject(key));
            if(conjugation.has())
                tenses.put(key, func.apply(this, obj.getJSONObject(key)));
        }
        else
            tenses.put(key, func.apply(this, new JSONObject()));
    }

    private void addOptionalTense(JSONObject obj, String key, BiFunction<CornishVerb, JSONObject, CornishVerbTense> func){
        if(obj.has(key)) {
            Conjugation conjugation = func.apply(this, obj.getJSONObject(key));
            if(conjugation.has())
                tenses.put(key, func.apply(this, obj.getJSONObject(key)));
        }
    }

    public String getStem(Affection affection){
        if(affection == Affection.BASE)
            return stem;
        if(affection == Affection.RAISED)
            return raisedStem;
        return generateAffectionStem(stem, affection);
    }

    public String getStem() {
        return stem;
    }

    public Affection getInfinitiveAffection() {
        return infinitiveAffection;
    }

    @Override
    protected JSONObject getConjugations() {
        JSONObject obj =  new JSONObject();
        for(String key : tenses.keySet())
            obj.put(key, tenses.get(key).toJson());
        return obj;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        for(Conjugation conjugation : tenses.values())
            list.addAll(conjugation.getVersions());
        return list;
    }
}
