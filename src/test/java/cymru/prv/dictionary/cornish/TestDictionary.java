package cymru.prv.dictionary.cornish;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * To ensure that initialization works as intended
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class TestDictionary {

    @Test
    public void testInitialization(){
        Assertions.assertDoesNotThrow(
                (ThrowingSupplier<CornishDictionary>) CornishDictionary::new,
                "Failed to initialize dictionary"
        );
    }

    @Test
    public void ensureThatVersionNumbersAreTheSame() throws IOException {
        for(var line : Files.readAllLines(Path.of( "build.gradle"))){
            if(line.startsWith("version")){
                String version = line
                        .split("\\s+")[1]
                        .replace("'", "");
                Assertions.assertEquals(version, new CornishDictionary().getVersion());
                return;
            }
        }
        Assertions.fail("Version number not found in build.gradle");
    }

}
