package cymru.prv.dictionary.cornish.tenses;

import cymru.prv.dictionary.common.Conjugation;
import cymru.prv.dictionary.cornish.Affection;
import cymru.prv.dictionary.cornish.CornishVerb;
import org.json.JSONObject;

public abstract class CornishVerbTense extends Conjugation {

    protected String stem;
    protected String raisedStem;
    protected boolean useSecondForm;
    protected boolean endsInYa;

    public CornishVerbTense(CornishVerb verb, JSONObject obj) {
        super(obj);
        stem = obj.optString("stem", verb.getStem());
        endsInYa = verb.getNormalForm().endsWith("ya");
        useSecondForm = obj.optBoolean("useSecondForm", useSecondForm(verb));
        raisedStem = verb.getStem(Affection.RAISED);
    }

    protected boolean useSecondForm(CornishVerb verb){
        return false;
    }

    protected String apply(String stem, String suffix){
        return (stem + suffix).replaceAll("(.)(\\1)(\\1|s)","$1$3"); //mmm mms -> $1$3
    }

    protected String apply(String suffix) {
        return apply(stem, suffix);
    }

    protected String applyWithAffectation(String suffix){
        if(raisedStem != null)
            return apply(raisedStem, suffix);
        return apply(suffix);
    }

}
