package cymru.prv.dictionary.cornish;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;

import java.util.List;


/**
 * A class to represent a standard Cornish adjective
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class CornishAdjective extends CornishWord {

    private static final String EQUATIVE = "equative";
    private static final String COMPARATIVE = "comparative";
    private static final String SUPERLATIVE = "superlative";

    private final boolean comparable;

    private final List<String> equative;
    private final List<String> comparative;
    private final List<String> superlative;

    public CornishAdjective(JSONObject obj) {
        super(obj, WordType.adjective);

        comparable = obj.optBoolean("comparable", true);
        if(comparable) {
            equative = obj.has(EQUATIVE) ? Json.getStringList(obj, EQUATIVE) : null;
            comparative = obj.has(COMPARATIVE) ? Json.getStringList(obj, COMPARATIVE) : null;
            superlative = obj.has(SUPERLATIVE) ? Json.getStringList(obj, SUPERLATIVE) : null;
        }
        else {
            equative = comparative = superlative = null;
        }
    }

    @Override
    protected JSONObject getInflections() {
        if(!comparable)
            return null;
        JSONObject obj = new JSONObject();
        addIfExist(obj, EQUATIVE, equative);
        addIfExist(obj, COMPARATIVE, comparative);
        addIfExist(obj, SUPERLATIVE, superlative);
        return obj.length() == 0 ? null : obj;
    }

    @Override
    public List<String> getVersions() {
        var versions = super.getVersions();
        if(equative != null)
            versions.addAll(equative);
        if(comparative != null)
            versions.addAll(comparative);
        if(superlative != null)
            versions.addAll(superlative);
        return versions;
    }
}
